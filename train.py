# Written by Flavius Radu Modan 2022
# Based on implementation from https://github.com/pranz24/pytorch-soft-actor-critic
# Main script that trains the network 
import argparse
import datetime
import gym
import numpy as np
import itertools
import torch
from lib.algo.sac import SAC
from torch.utils.tensorboard import SummaryWriter
from lib.replay_memory import ReplayMemory
import arm_and_pole
from config import *
from tqdm import tqdm


def train_on_episode(env,agent,updates,episode_num,random_actions,render):
    episode_reward = 0
    episode_steps = 0
    done = False
    state = env.reset()
    critic_1_loss,critic_2_loss,policy_loss,ent_loss,alpha = None,None,None,None,None
    did_param_update = False
    env_data = []
    while not done:
        if render:
            env.render()
        
        if random_actions:
            action = env.action_space.sample()  # Sample random action
        else:
            action = agent.select_action(state)  # Sample action from policy

        if len(agent.memory) > 2*BATCH_SIZE:
            # Number of updates per step in environment
            for i in range(UPDATES_PER_STEP):
                # Update parameters of all the networks
                critic_1_loss, critic_2_loss, policy_loss, ent_loss, alpha = agent.update_parameters(BATCH_SIZE,updates)
                updates += 1
                did_param_update = True

        next_state, reward, done, _ = env.step(action) # Step
        episode_steps += 1
        episode_reward += reward
        mask = 1 if episode_steps == env.env._max_episode_steps else float(not done)
        env_data.append((state, action, reward, next_state, mask))
        state = next_state
    
    for state_data in env_data:
        agent.push_to_memory(state_data[0], state_data[1], state_data[2], state_data[3], state_data[4],episode_reward) # Append transition to memory
    agent.increment_episode_count()
    return episode_reward, episode_steps,updates ,critic_1_loss,critic_2_loss,policy_loss,ent_loss,alpha,did_param_update

def play_episode(env,agent):
    state = env.reset()
    episode_reward = 0
    done = False
    while not done:
        if RENDER_EVAL:
            env.render()
        action = agent.select_action(state, evaluate=True)
        next_state, reward, done, _ = env.step(action)
        episode_reward += reward
        state = next_state
    return episode_reward

def train_on_env(env,agent,episodes,updates,total_ep,writer,random_actions,render,do_eval):
    # Training Loop
    for i in (progress_bar := tqdm(range(1,episodes+1))):
        ep_reward, ep_steps, updates,critic_1_loss,critic_2_loss,policy_loss,ent_loss,alpha,param_update = train_on_episode(env=env, agent=agent,updates=updates,episode_num=total_ep,random_actions=random_actions,render = ( ))
        total_ep += 1
        writer.add_scalar('reward/train', ep_reward,total_ep)
        progress_bar.set_postfix({"reward_last_train":ep_reward})
        progress_bar.refresh()
        if param_update:
            writer.add_scalar('loss/critic_1', critic_1_loss, updates)
            writer.add_scalar('loss/critic_2', critic_2_loss, updates)
            writer.add_scalar('loss/policy', policy_loss, updates)
            writer.add_scalar('loss/entropy_loss', ent_loss, updates)
            writer.add_scalar('entropy_temprature/alpha', alpha, updates)
        
        if i % SAVE_FREQ == 0 and do_eval:
            avg_reward = 0
            eval_episodes = EPISODES_FOR_EVAL
            for _  in range(eval_episodes):
                avg_reward += play_episode(env, agent)
            avg_reward /= eval_episodes
            agent.save_checkpoint(env_name=ENV_NAME,suffix=f'{avg_reward}_SAC_{EXPERIMENT_NICKNAME}_{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}')
            writer.add_scalar('avg_reward/test', avg_reward, total_ep - START_GAMES)
            progress_bar.set_postfix({"reward_last_eval":avg_reward})
            progress_bar.refresh()
    return updates,total_ep

def main():
    env = gym.make(ENV_NAME)

    env.seed(SEED)
    env.action_space.seed(SEED)

    torch.manual_seed(SEED)
    np.random.seed(SEED)

    # Memory
    memory = ReplayMemory(REPLAY_SIZE, SEED)

    # Agent
    agent:SAC = SAC(env.observation_space.shape[0], env.action_space,memory)
    if LOAD_CKPT_ON_TRAIN:
        agent.load_checkpoint(ckpt_folder=TRAIN_LOAD_PATH)

    #Tesnorboard
    writer = SummaryWriter(save_folder,save_folder)

    updates = 0
    total_ep = 0
    if not LOAD_CKPT_ON_TRAIN:
        updates,total_ep = train_on_env(env, agent, START_GAMES, updates, total_ep, writer, True, False, EVAL_RANDOM)
    updates,total_ep = train_on_env(env, agent, NUM_GAMES-START_GAMES, updates, total_ep, writer, False, RENDER_EVAL, EVAL)

    env.close()

if __name__ == "__main__":
    main()

