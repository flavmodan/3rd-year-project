# Based on the original implementation of the mujoco cart-pole env from openai
# Written by Flavius Modan Radu 2022
import os

import numpy as np
from gym import error, spaces, utils
from gym.envs.mujoco import mujoco_env
from mujoco_py import MjViewer, functions


class TestEnvClass(mujoco_env.MujocoEnv, utils.EzPickle):
    def __init__(self):
        utils.EzPickle.__init__(self)
        FILE_PATH = os.path.join(os.path.dirname(__file__), "mujoco_setup.xml")
        frame_skip = 3
        mujoco_env.MujocoEnv.__init__(self, FILE_PATH, frame_skip)

    def step(self, a):
        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        notdone = (
            np.isfinite(ob).all() and (np.abs(ob[1]) <= 0.2) and (np.abs(ob[2]) <= 0.2)
        )
        done = not notdone
        reward = min((1.6 - abs(ob[1])) / 1.6, (1.6 - abs(ob[2])) / 1.6)
        reward = 0.0 if reward < 0.0 else reward
        return ob, reward, done, {}

    def reset_model(self):
        qpos = self.init_qpos + self.np_random.uniform(
            size=self.model.nq, low=-0.01, high=0.01
        )
        qvel = self.init_qvel + self.np_random.uniform(
            size=self.model.nv, low=-0.01, high=0.01
        )
        self.set_state(qpos, qvel)
        return self._get_obs()

    def _get_obs(self):
        return np.concatenate([self.sim.data.qpos, self.sim.data.qvel]).ravel()

    def viewer_setup(self):
        v = self.viewer
        v.cam.trackbodyid = 0
        v.cam.distance = self.model.stat.extent
