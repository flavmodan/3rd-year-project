from gym.envs.registration import register

register(
    id="MujocoTestEnv-v0",
    entry_point="mujoco_test_env.envs:TestEnvClass",
)
