from gym.envs.registration import register

register(
    id="ArmAndPole-v0",
    entry_point="arm_and_pole.envs:ArmAndPoleEnvClass",
)
