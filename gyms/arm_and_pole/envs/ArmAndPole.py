# Written by Flavius Radu Modan 2022
import math
import os
from dis import dis
from tkinter.tix import Tree
from turtle import distance

import numpy as np
from gym import error, spaces, utils
from gym.envs.mujoco import mujoco_env
from mujoco_py import MjViewer, functions
from scipy.spatial.transform import Rotation as R



class ArmAndPoleEnvClass(mujoco_env.MujocoEnv, utils.EzPickle):

    POINT_OF_NO_RETURN_ANGULAR = 0.8
    POINT_OF_NO_RETURN_JOINT = 0.95
    MAX_CART_JOINT = np.pi/2
    POINT_OF_NO_RETURN_ANGULAR_INIT = 0.3
    POINT_OF_NO_RETURN_VEL = 40
    POLE_POSITION_RANDOM_MAG = 1e-2
    POLE_VELOCITY_RANDOM_MAG = 1e-1
    RANDOMIZE_POLE_POS_AND_VEL = True
    DIST_IMPORTANCE = 1e-3
    ANGLE_IMPORTANCE = 5e-1
    COLLISION_IMPORTANCE = 0
    MAX_DIST = 1.3
    MAX_ANGLE = 90
    MAX_RANDOMIZATION = 20
    ANGLE_DECAY = False
    ANGLE_DECAY_EPISODES = 500
    ANGLE_DECAY_MAG = 0.002
    ANGLE_DECAY_MIN_ANGLE = 0
    POLE_HITS_WRIST_CONSTANT = 0.92
    INIT_QPOS = np.array([1.36625756e-08 ,1.11720240e+00 - 0.5 ,3.18206720e-02,-1.84670668e+00 + 0.5,-8.10997376e-02 ,1.16496607 - 0.5 ,1.97708005e-03 + 0.2 ,0 ,0])
    INIT_QVEL = np.array([0.0]*9)
    init_pole_pos = np.array([None])
    init_pole_angle = np.array([360,170,90])
    _max_episode_steps = 1e5

    def __init__(self):
        utils.EzPickle.__init__(self)
        FILE_PATH = os.path.join(os.path.dirname(__file__), "sawyer_sim/sawyer.xml")
        frame_skip = 1
        self.interations = 0
        self.step_cnt = 0
        mujoco_env.MujocoEnv.__init__(self, FILE_PATH, frame_skip)
        self.init_qpos = self.INIT_QPOS
        self.init_qvel = self.INIT_QVEL


    def reward(self):
        dist = self.get_pole_dist()
        linear_deviation = min(dist,self.MAX_DIST)
        linear_deviation /= self.MAX_DIST
        angle_deviation = self.get_pole_deviation()
        collision = float(self.sim.data.ncon >= 1)
        reward = 1.0 - self.ANGLE_IMPORTANCE * angle_deviation - self.DIST_IMPORTANCE * linear_deviation - self.COLLISION_IMPORTANCE * collision
        if reward > 0.9:
            print("",end="")
            pass
        return reward
    
    def get_pole_rot(self):
        pole_quat = self.sim.data.get_body_ximat("pole")
        return R.from_matrix(pole_quat).as_euler("xyz",degrees=True) + np.array([180]*3)


    def get_pole_deviation(self):
        current_rot = self.get_pole_rot()
        dist = np.abs(current_rot - self.init_pole_angle)
        current_rot_dist = [ np.min([d,360-d]) for d in dist]
        deviation_angle = np.min([np.max(current_rot_dist),self.MAX_ANGLE])
        deviation = math.sin(math.radians(deviation_angle))
        return deviation
        # return x_angle,z_angle

    def get_pole_dist(self):
        if self.init_pole_pos.any() == None:
            return 0.0
        pole_qpos = self.sim.data.get_body_xpos("pole")
        current_pos = np.array(pole_qpos)
        dist = np.sqrt(np.sum((self.init_pole_pos - current_pos) ** 2, axis=0))
        return dist
    
    def is_not_done(self,is_for_init=False):
        numcon = self.sim.data.ncon
        angle_deviation = self.get_pole_deviation()
        z_joint_angle = self.sim.data.get_sensor("z_hinge_pos")
        x_joint_angle = self.sim.data.get_sensor("x_hinge_pos")
        if x_joint_angle > self.POLE_HITS_WRIST_CONSTANT:
            x_joint_angle = self.MAX_CART_JOINT
        max_joint_angle_norm = np.max([np.abs(x_joint_angle),np.abs(z_joint_angle)])/self.MAX_CART_JOINT
        pole_qvel_z = self.sim.data.get_joint_qvel("hinge1")
        pole_qvel_x = self.sim.data.get_joint_qvel("hinge2")
        acceptable_angle_diviation = (self.POINT_OF_NO_RETURN_ANGULAR if not is_for_init else self.POINT_OF_NO_RETURN_ANGULAR_INIT)
        return (max(np.abs(pole_qvel_z),np.abs(pole_qvel_x)) <= self.POINT_OF_NO_RETURN_VEL) and ( angle_deviation <= acceptable_angle_diviation) and numcon != 1 and max_joint_angle_norm <= self.POINT_OF_NO_RETURN_JOINT

    def step(self, a):
        self.step_cnt += 1
        self.do_simulation(a, self.frame_skip)
        ob = self._get_obs()
        reward = self.reward()
        notdone = self.is_not_done()
        done = not notdone
        if self.step_cnt == 1:
            done = False
        if self.step_cnt == self._max_episode_steps:
            done = True
        self.sim.data.solver_iter = self.interations
        return ob, reward, done, {}
    
    def reset_model(self):
        self.step_cnt = 0
        self.init_qpos = self.INIT_QPOS
        qpos = self.init_qpos
        qvel = self.init_qvel
        self.set_state(qpos, qvel)
        if self.RANDOMIZE_POLE_POS_AND_VEL:
            qvel[-2:] = self.INIT_QVEL[-2:] + self.np_random.uniform(
                size=2, low=-self.POLE_VELOCITY_RANDOM_MAG, high=self.POLE_VELOCITY_RANDOM_MAG
            )
            qpos[-2:] = self.INIT_QPOS[-2:] + self.np_random.uniform(
                size=2, low=-self.POLE_POSITION_RANDOM_MAG, high=self.POLE_POSITION_RANDOM_MAG
            )
        self.set_state(qpos, qvel)
        self.init_pole_pos = np.array(self.sim.data.get_body_xpos("pole"))
        self.decay_max_angle()
        self.interations +=1
        return self._get_obs()

    def _get_obs(self):
        pole_quat = self.sim.data.get_body_xquat("pole")
        pole_qvelr = self.sim.data.get_body_xvelr("pole")
        return np.concatenate([pole_quat, pole_qvelr,self.sim.data.qpos, self.sim.data.qvel]).ravel()

    def viewer_setup(self):
        v = self.viewer
        v.cam.trackbodyid = 0
        v.cam.distance = self.model.stat.extent
    
    def decay_max_angle(self):
        if (self.interations % self.ANGLE_DECAY_EPISODES == 0) and (self.interations != 0) and (self.POINT_OF_NO_RETURN_ANGULAR > self.ANGLE_DECAY_MIN_ANGLE) and (self.ANGLE_DECAY):
            self.POINT_OF_NO_RETURN_DEG -= self.ANGLE_DECAY_MAG

