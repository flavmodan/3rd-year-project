# Based on implementation from https://github.com/pranz24/pytorch-soft-actor-critic
# Modified by Flavius Radu Modan 2022
import random
import numpy as np
import os
import pickle
from config import *
class ReplayMemory:
    def __init__(self, capacity, seed):
        random.seed(seed)
        self.capacity = capacity
        self.memory_buffer = []
        self.index = 0
        self.episode_count = 0
        self.seed = seed

    def increment_episode_count(self):
        self.episode_count+=1

    def push(self, state, action, reward, next_state, done,episode_reward):
        if len(self.memory_buffer) < self.capacity:
            self.memory_buffer.append(None)
        self.memory_buffer[self.index] = (state, action, reward, next_state, done, episode_reward)
        self.index = (self.index + 1) % self.capacity

    def sample(self, batch_size):
        batch = random.sample(self.memory_buffer, batch_size)
        state, action, reward, next_state, done, episode_reward = map(np.stack, zip(*batch))
        return state, action, reward, next_state, done, episode_reward
    
    def sample_last_entry(self):
        batch = self.memory_buffer[self.index - 1]
        state, action, reward, next_state, done, episode_reward = batch
        return state, action, np.array(reward), next_state, np.array(done), np.array(episode_reward)

    def __len__(self):
        return len(self.memory_buffer)

    def save_buffer(self, save_folder="checkpoints/", save_name=None):
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        if save_name is None:
            save_name = BUFFER_CKPT_DEFAULT_NAME
        save_path = save_folder+save_name

        with open(save_path, 'wb') as f:
            pickle.dump(self.memory_buffer, f)

    def load_buffer(self, save_path):
        print('Loading buffer from {}'.format(save_path))

        with open(save_path, "rb") as f:
            self.memory_buffer = pickle.load(f)
            self.index = len(self.memory_buffer) % self.capacity
