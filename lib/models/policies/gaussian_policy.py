# Taken from implementation of GaussianPolicy from https://github.com/pranz24/pytorch-soft-actor-critic
# For comparison purposes this has been left mostly as is
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal
from lib.utils import weights_init_
from config import *

class GaussianPolicy(nn.Module):
    def __init__(self, num_inputs, hidden_dim, action_space):
        super(GaussianPolicy, self).__init__()
        num_actions = action_space.shape[0]
        
        self.l1 = nn.Linear(num_inputs, hidden_dim)
        self.l2 = nn.Linear(hidden_dim, hidden_dim)

        self.mean_output = nn.Linear(hidden_dim, num_actions)
        self.log_std_output = nn.Linear(hidden_dim, num_actions)

        self.apply(weights_init_)

        # action rescaling
        if action_space is None:
            self.action_scale = torch.tensor(1.)
            self.action_bias = torch.tensor(0.)
        else:
            self.action_scale = torch.FloatTensor(
                (action_space.high - action_space.low) / 2.)
            self.action_bias = torch.FloatTensor(
                (action_space.high + action_space.low) / 2.)

    def forward(self, state):
        x = F.relu(self.l1(state))
        x = F.relu(self.l2(x))
        mean = self.mean_output(x)
        log_std = self.log_std_output(x)
        log_std = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        return mean, log_std

    def sample(self, state):
        mean, log_std = self.forward(state)
        std = log_std.exp()
        normal = Normal(mean, std)
        sampl = normal.rsample()  # for reparameterization trick (mean + std * N(0,1))
        pi = torch.tanh(sampl)
        action = pi * self.action_scale + self.action_bias
        log_pi = normal.log_prob(sampl)
        # Enforcing Action Bound
        log_pi -= torch.log(self.action_scale * (1 - pi.pow(2)) + epsilon)
        log_pi = log_pi.sum(1, keepdim=True)
        mean = torch.tanh(mean) * self.action_scale + self.action_bias
        return action, log_pi, mean

    def to(self, device):
        self.action_scale = self.action_scale.to(device)
        self.action_bias = self.action_bias.to(device)
        return super(GaussianPolicy, self).to(device)