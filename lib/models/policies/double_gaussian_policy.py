# Based on implementation of GaussianPolicy from https://github.com/pranz24/pytorch-soft-actor-critic
# Adapted to a double policy by Flavius Radu Modan 2022
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal
from lib.utils import weights_init_
from config import *

class DoubleGaussianPolicy(nn.Module):
    def __init__(self, num_inputs, hidden_dim, action_space):
        super(DoubleGaussianPolicy, self).__init__()
        num_actions = action_space.shape[0]
        
        self.p1_l1 = nn.Linear(num_inputs, hidden_dim)
        self.p1_l2 = nn.Linear(hidden_dim, hidden_dim)

        self.p1_mean_output = nn.Linear(hidden_dim, num_actions)
        self.p1_log_std_output = nn.Linear(hidden_dim, num_actions)

        self.p2_l1 = nn.Linear(num_inputs, hidden_dim)
        self.p2_l2 = nn.Linear(hidden_dim, hidden_dim)

        self.p2_mean_output = nn.Linear(hidden_dim, num_actions)
        self.p2_log_std_output = nn.Linear(hidden_dim, num_actions)

        self.apply(weights_init_)

        # action rescaling
        if action_space is None:
            self.action_scale = torch.tensor(1.)
            self.action_bias = torch.tensor(0.)
        else:
            self.action_scale = torch.FloatTensor(
                (action_space.high - action_space.low) / 2.)
            self.action_bias = torch.FloatTensor(
                (action_space.high + action_space.low) / 2.)

    def forward(self, state):
        x = F.relu(self.p1_l1(state))
        x = F.relu(self.p1_l2(x))
        mean_1 = self.p1_mean_output(x)
        log_std = self.p1_log_std_output(x)
        log_std_1 = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        
        x = F.relu(self.p2_l1(state))
        x = F.relu(self.p2_l2(x))
        mean_2 = self.p2_mean_output(x)
        log_std = self.p2_log_std_output(x)
        log_std_2 = torch.clamp(log_std, min=LOG_SIG_MIN, max=LOG_SIG_MAX)
        
        return mean_1, log_std_1, mean_2 ,log_std_2

    def make_action(self,mean,std):
        normal = Normal(mean, std)
        sampl = normal.rsample()  # for reparameterization trick (mean + std * N(0,1))
        pi = torch.tanh(sampl)
        action = pi * self.action_scale + self.action_bias
        log_pi = normal.log_prob(sampl)
        # Enforcing Action Bound
        log_pi -= torch.log(self.action_scale * (1 - pi.pow(2)) + epsilon)
        log_pi = log_pi.sum(1, keepdim=True)
        mean = torch.tanh(mean) * self.action_scale + self.action_bias
        return action, log_pi, mean

    def sample(self, state):
        mean_1, log_std_1, mean_2, log_std_2 = self.forward(state)
        std_1 = log_std_1.exp()
        std_2 = log_std_2.exp()
        action_1, log_pi_1, mean_1 = self.make_action(mean_1, std_1)
        action_2, log_pi_2, mean_2 = self.make_action(mean_2, std_2)
        
        mean_log_1 = torch.mean(log_pi_1)
        mean_log_2 = torch.mean(log_pi_2)

        if mean_log_1 > mean_log_2:
            return action_1,log_pi_1,mean_1
        return action_2,log_pi_2,mean_2

    def to(self, device):
        self.action_scale = self.action_scale.to(device)
        self.action_bias = self.action_bias.to(device)
        return super(DoubleGaussianPolicy, self).to(device)