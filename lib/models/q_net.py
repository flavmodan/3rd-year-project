# Based on implementation from https://github.com/pranz24/pytorch-soft-actor-critic
# Modified by Flavius Radu Modan 2022
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal
from lib.utils import weights_init_

class QNetwork(nn.Module):
    def __init__(self, num_inputs, num_actions, hidden_dim):
        super(QNetwork, self).__init__()

        # Q1 architecture
        self.q1_l1 = nn.Linear(num_inputs + num_actions, hidden_dim)
        self.q1_l2 = nn.Linear(hidden_dim, hidden_dim)
        self.q1_l3 = nn.Linear(hidden_dim, 1)

        # Q2 architecture
        self.q2_l1 = nn.Linear(num_inputs + num_actions, hidden_dim)
        self.q2_l2 = nn.Linear(hidden_dim, hidden_dim)
        self.q2_l3 = nn.Linear(hidden_dim, 1)

        self.apply(weights_init_)

    def forward(self, state, action):
        xu = torch.cat([state, action], 1)
        
        x1 = F.relu(self.q1_l1(xu))
        x1 = F.relu(self.q1_l2(x1))
        x1 = self.q1_l3(x1)

        x2 = F.relu(self.q2_l1(xu))
        x2 = F.relu(self.q2_l2(x2))
        x2 = self.q2_l3(x2)

        return x1, x2