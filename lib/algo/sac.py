# Based on implementation from https://github.com/pranz24/pytorch-soft-actor-critic
# Adapted by Flavius Radu Modan 2022
# This has been adapted to use the enhancements from other papers
import os
import torch
import torch.nn.functional as F
from torch.optim import Adam
from lib.utils import soft_update, hard_update
from lib.models.policies.gaussian_policy import GaussianPolicy
from lib.models.policies.deterministic_policy import DeterministicPolicy
from lib.models.policies.double_gaussian_policy import DoubleGaussianPolicy
from lib.models.q_net import QNetwork
from lib.replay_memory import ReplayMemory
from config import *
import numpy as np
class SAC(object):
    def __init__(self, num_inputs, action_space,memory):

        self.gamma = GAMMA
        self.tau = TAU
        self.alpha = ALPHA

        self.policy_type = policy
        self.target_update_interval = TARGET_UPDATE_INTERVAL
        self.automatic_entropy_tuning = AUTOMATIC_ENTROPY

        self.device = torch.device("cuda")

        self.critic = QNetwork(num_inputs, action_space.shape[0], HIDDEN_SIZE).to(device=self.device)
        self.critic_optim = Adam(self.critic.parameters(), lr=LR)

        self.critic_target = QNetwork(num_inputs, action_space.shape[0], HIDDEN_SIZE).to(self.device)
        hard_update(self.critic_target, self.critic)
        self.memory:ReplayMemory = memory
        if USE_DELAYED_BUFFER:
            self.temp_memory:ReplayMemory = ReplayMemory(self.memory.capacity, self.memory.seed)

        if self.policy_type == "Gaussian":
            # Target Entropy = −dim(A) (e.g. , -6 for HalfCheetah-v2) as given in the paper
            if self.automatic_entropy_tuning is True:
                self.target_entropy = -torch.prod(torch.Tensor(action_space.shape).to(self.device)).item()
                self.log_alpha = torch.zeros(1, requires_grad=True, device=self.device)
                self.alpha_optim = Adam([self.log_alpha], lr=LR)

            self.policy = DoubleGaussianPolicy(num_inputs, HIDDEN_SIZE, action_space).to(self.device) if USE_DOUBLE_VER else GaussianPolicy(num_inputs, HIDDEN_SIZE, action_space).to(self.device) 
            self.policy_optim = Adam(self.policy.parameters(), lr=LR)

        else:
            self.alpha = 0
            self.automatic_entropy_tuning = False
            self.policy = DeterministicPolicy(num_inputs, HIDDEN_SIZE, action_space).to(self.device)
            self.policy_optim = Adam(self.policy.parameters(), lr=LR)

    def select_action(self, state, evaluate=False):
        state = torch.FloatTensor(state).to(self.device).unsqueeze(0)
        if evaluate is False:
            action, _, _ = self.policy.sample(state)
        else:
            _, _, action = self.policy.sample(state)
        return action.detach().cpu().numpy()[0]
    
    def get_batch_from_mem_direct(self,batch_size):
        state_batch, action_batch, reward_batch, next_state_batch, not_done_batch, episode_reward_batch = self.memory.sample(batch_size=batch_size)

        state_batch = torch.FloatTensor(state_batch).to(self.device)
        next_state_batch = torch.FloatTensor(next_state_batch).to(self.device)
        action_batch = torch.FloatTensor(action_batch).to(self.device)
        reward_batch = torch.FloatTensor(reward_batch).to(self.device).unsqueeze(1)
        not_done_batch = torch.FloatTensor(not_done_batch).to(self.device).unsqueeze(1)
        episode_reward_batch = torch.FloatTensor(episode_reward_batch).to(self.device).unsqueeze(1)

        return (state_batch,next_state_batch,action_batch,reward_batch,not_done_batch,episode_reward_batch)
    
    def get_last_state_from_mem(self,memory:ReplayMemory):
        state_batch, action_batch, reward_batch, next_state_batch, not_done_batch, episode_reward_batch = memory.sample_last_entry()

        state_batch = torch.FloatTensor(state_batch).to(self.device)
        next_state_batch = torch.FloatTensor(next_state_batch).to(self.device)
        action_batch = torch.FloatTensor(action_batch).to(self.device)
        reward_batch = torch.FloatTensor(reward_batch).to(self.device)
        not_done_batch = torch.FloatTensor(not_done_batch).to(self.device)
        episode_reward_batch = torch.FloatTensor(episode_reward_batch).to(self.device)

        return (state_batch,next_state_batch,action_batch,reward_batch,not_done_batch,episode_reward_batch)

    def increment_episode_count(self):
        if USE_DELAYED_BUFFER:
            self.temp_memory.increment_episode_count()
        else:
            self.memory.increment_episode_count()
    
    def push_to_memory(self, state, action, reward, next_state, done,episode_reward):
        if USE_DELAYED_BUFFER:
            self.temp_memory.push(state, action, reward, next_state, done, episode_reward)
            if self.temp_memory.episode_count == DELAYED_BUFFER_EPISODE_SIZE:
                for sample in self.temp_memory.memory_buffer:
                    self.memory.push(*sample)
                self.memory.episode_count+=self.temp_memory.episode_count
                self.temp_memory:ReplayMemory = ReplayMemory(self.memory.capacity, self.memory.seed)
        else:
            self.memory.push(state, action, reward, next_state, done, episode_reward)
    
    def get_priority_memory_batch(self,batch_size):
        state_batch,next_state_batch,action_batch,reward_batch,not_done_batch,episode_reward_batch = self.get_batch_from_mem_direct(batch_size)
        batch_1 = {}
        batch_1["states"] = state_batch
        batch_1["next_states"] = next_state_batch
        batch_1["actions"] = action_batch
        batch_1["rewards"] = reward_batch
        batch_1["not_done"] = not_done_batch
        batch_1["episode_reward"] = episode_reward_batch
        state_batch,next_state_batch,action_batch,reward_batch,not_done_batch,episode_reward_batch = self.get_batch_from_mem_direct(batch_size)
        batch_2 = {}
        batch_2["states"] = state_batch
        batch_2["next_states"] = next_state_batch
        batch_2["actions"] = action_batch
        batch_2["rewards"] = reward_batch
        batch_2["not_done"] = not_done_batch
        batch_2["episode_reward"] = episode_reward_batch
        
        batch_mixed = []
        for i in range(batch_size):
            batch_dict = {}
            for key in list(batch_1.keys()):
                batch_dict[key] = batch_1[key][i]
            batch_mixed.append(batch_dict)
            batch_dict = {}
            for key in list(batch_2.keys()):
                batch_dict[key] = batch_2[key][i]
            batch_mixed.append(batch_dict)
        batch_mixed = sorted(batch_mixed,key = lambda batch: batch["episode_reward"],reverse=True)
        max_batch = batch_mixed[:batch_size]
        min_batch = batch_mixed[batch_size:]
        np.random.shuffle(max_batch)
        np.random.shuffle(min_batch)
        # max_batch_ep_rewards = torch.stack([batch["episode_reward"] for batch in max_batch])
        # min_batch_ep_rewards = torch.stack([batch["episode_reward"] for batch in min_batch])
        
        final_batches={}
        final_batches["min"] = {}
        final_batches["max"] = {}
        for key in list(batch_1.keys()):
            final_batches["min"][key] = torch.stack([batch[key] for batch in min_batch])
            final_batches["max"][key] = torch.stack([batch[key] for batch in max_batch])

        cos_sim = torch.nn.CosineSimilarity(dim=0,eps=1e-6)
        simmilarity = cos_sim(batch_1["episode_reward"],batch_2["episode_reward"])
        if simmilarity>BATCH_DISTANCE_TH:
            coin = torch.randint(0, 2, (1,))
            if coin == 0:
                return [batch_1[key] for key in ["states","next_states","actions","rewards","not_done","episode_reward"]]
            else:
                return [batch_2[key] for key in ["states","next_states","actions","rewards","not_done","episode_reward"]]
        else:
            return [final_batches["max"][key] for key in ["states","next_states","actions","rewards","not_done","episode_reward"]]
    
    def get_train_batch(self,batch_size):
        if USE_BATCH_DISCRIMINATION:
            samples = self.get_priority_memory_batch(batch_size)
        else:
            samples = self.get_batch_from_mem_direct(batch_size)
        if not MIX_MOO_EXP:
            return samples
        idx = torch.randint(0, batch_size, (1,))
        if self.temp_memory.index >= 1:
            new_sampl = self.get_last_state_from_mem(self.temp_memory)
        else:
            new_sampl = self.get_last_state_from_mem(self.memory)
        for i in range(len(new_sampl)):
            samples[i][idx] = new_sampl[i]
        return samples


    def get_q_loss(self,state_batch,next_state_batch,action_batch,reward_batch,not_done_batch):
        
        with torch.no_grad():
            next_state_action, next_state_log_pi, _ = self.policy.sample(next_state_batch)
            q_value_1_next_target, q_value_2_next_target = self.critic_target(next_state_batch, next_state_action)
            min_q_value_next_target = torch.min(q_value_1_next_target, q_value_2_next_target) - self.alpha * next_state_log_pi
            next_q_value = reward_batch + not_done_batch * self.gamma * (min_q_value_next_target)
        
        q_value_1_current_state, q_value_2_current_state = self.critic(state_batch, action_batch)  # Two Q-functions to mitigate positive bias in the policy improvement step
        q_func_1_loss = F.mse_loss(q_value_1_current_state, next_q_value)  # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q_func_2_loss = F.mse_loss(q_value_2_current_state, next_q_value)  # JQ = 𝔼(st,at)~D[0.5(Q1(st,at) - r(st,at) - γ(𝔼st+1~p[V(st+1)]))^2]
        q_func_loss = q_func_1_loss + q_func_2_loss
        return q_func_loss, q_func_1_loss, q_func_2_loss

    def get_policy_loss(self,state_batch):
        pi, log_pi, _ = self.policy.sample(state_batch)

        q_value_1_pi, q_value_2_pi = self.critic(state_batch, pi)
        min_q_value_pi = torch.min(q_value_1_pi, q_value_2_pi)

        policy_loss = ((self.alpha * log_pi) - min_q_value_pi).mean() # Jπ = 𝔼st∼D,εt∼N[α * logπ(f(εt;st)|st) − Q(st,f(εt;st))]
        return policy_loss, log_pi

    def get_entropy_loss(self, log_pi):
        alpha_loss = -(self.log_alpha * (log_pi + self.target_entropy).detach()).mean()
        return alpha_loss

    def update_parameters(self, batch_size, total_steps):
        # Sample a batch from memory
        state_batch,next_state_batch,action_batch,reward_batch,not_done_batch,_ = self.get_train_batch(batch_size)

        q_func_loss, q_func_1_loss, q_func_2_loss = self.get_q_loss(state_batch, next_state_batch, action_batch, reward_batch, not_done_batch)

        self.critic_optim.zero_grad()
        q_func_loss.backward()
        self.critic_optim.step()

        policy_loss, batch_log_pi = self.get_policy_loss(state_batch)

        self.policy_optim.zero_grad()
        policy_loss.backward()
        self.policy_optim.step()

        if self.automatic_entropy_tuning:
            
            alpha_loss = self.get_entropy_loss(batch_log_pi)

            self.alpha_optim.zero_grad()
            alpha_loss.backward()
            self.alpha_optim.step()
            self.alpha = self.log_alpha.exp()
            if CAP_ENTROPY_TEMP:
                self.alpha = torch.clamp(self.alpha,min=ENTROPY_TEMP_MIN,max=ENTROPY_TEMP_MAX)
            alpha_tlogs = self.alpha.clone() # For TensorboardX logs
        else:
            alpha_loss = torch.tensor(0.).to(self.device)
            alpha_tlogs = torch.tensor(self.alpha) # For TensorboardX logs


        if total_steps % self.target_update_interval == 0:
            soft_update(self.critic_target, self.critic, self.tau)

        return q_func_1_loss.item(), q_func_2_loss.item(), policy_loss.item(), alpha_loss.item(), alpha_tlogs.item()

    # Save model parameters
    def save_checkpoint(self, env_name, suffix="", ckpt_name=None,with_memory=SAVE_BUFFER_IN_CHECKPOINTS,parent_folder=PARENT_FOLDER_FOR_CHECKPOINTS):
        if ckpt_name is None:
            ckpt_name = "sac_checkpoint_{}_{}".format(env_name, suffix)
        ckpt_folder = parent_folder+ckpt_name+"/"
        if not os.path.exists(ckpt_folder):
            os.makedirs(ckpt_folder)
        if with_memory:
            self.memory.save_buffer(save_folder=ckpt_folder)
        ckpt_path = ckpt_folder+MODEL_CKPT_DEFAULT_NAME
        save_dict = {'policy_state_dict': self.policy.state_dict(),
                    'critic_state_dict': self.critic.state_dict(),
                    'critic_target_state_dict': self.critic_target.state_dict(),
                    'critic_optimizer_state_dict': self.critic_optim.state_dict(),
                    'policy_optimizer_state_dict': self.policy_optim.state_dict()}
        if self.automatic_entropy_tuning:
            save_dict["alpha_state"] = self.log_alpha.clone()
            save_dict["alpha_optimizer_state_dict"] = self.alpha_optim.state_dict()
        torch.save(save_dict, ckpt_path)

    # Load model parameters
    def load_checkpoint(self, ckpt_folder, evaluate=False,with_memory=SAVE_BUFFER_IN_CHECKPOINTS,model_ckpt_name=MODEL_CKPT_DEFAULT_NAME,buffer_ckpt_name=BUFFER_CKPT_DEFAULT_NAME):
        print('Loading models from {}'.format(ckpt_folder))
        if ckpt_folder is not None:
            if ckpt_folder[-1] != "/":
                ckpt_folder+="/"
            ckpt_model_path = ckpt_folder+model_ckpt_name
            checkpoint = torch.load(ckpt_model_path)
            self.policy.load_state_dict(checkpoint['policy_state_dict'])
            self.critic.load_state_dict(checkpoint['critic_state_dict'])
            self.critic_target.load_state_dict(checkpoint['critic_target_state_dict'])
            self.critic_optim.load_state_dict(checkpoint['critic_optimizer_state_dict'])
            self.policy_optim.load_state_dict(checkpoint['policy_optimizer_state_dict'])
            if self.automatic_entropy_tuning:
                self.log_alpha = checkpoint["alpha_state"].clone()
                self.alpha_optim.load_state_dict(checkpoint["alpha_optimizer_state_dict"])
            if with_memory:
                ckpt_buffer_path = ckpt_folder+buffer_ckpt_name
                self.memory.load_buffer(ckpt_buffer_path)
            if evaluate:
                self.policy.eval()
                self.critic.eval()
                self.critic_target.eval()
            else:
                self.policy.train()
                self.critic.train()
                self.critic_target.train()

