# Written by Flavius Radu Modan 2022
# Main evaluation script - loads a checkpoint and plays episodes 
import argparse
import datetime
import gym
import numpy as np
import itertools
import torch
from lib.algo.sac import SAC
from lib.replay_memory import ReplayMemory
import arm_and_pole
from config import *
from torch.utils.tensorboard import SummaryWriter

env = gym.make(ENV_NAME)
memory = ReplayMemory(REPLAY_SIZE, SEED)
agent = SAC(env.observation_space.shape[0], env.action_space,memory)
agent.load_checkpoint(ckpt_folder="checkpoints/sac_checkpoint_ArmAndPole-v0_6128.577177828854_SAC_prority_buffer_sampling_better_2022-03-26_13-57-39")
#Tesnorboard
writer = SummaryWriter(save_folder+"_eval",save_folder+"_eval")
total_ep = 0
while True:
    state = env.reset()
    episode_reward = 0
    done = False
    while not done:
        if RENDER_EVAL:
            env.render()
        action = agent.select_action(state, evaluate=True)

        next_state, reward, done, _ = env.step(action)
        episode_reward += reward
        state = next_state
    print(f"Episode reward {episode_reward}")
    writer.add_scalar('avg_reward/test', episode_reward,total_ep)
    total_ep += 1
