#!/usr/bin/python
# Config file for the algo 
# Written by Flavius Radu Modan 2022
import os

import arm_and_pole
import mujoco_test_env
import datetime

ENV_NAME = "ArmAndPole-v0"
GAUSSIAN = True
EVAL = True
EVAL_RANDOM = False
RENDER_EVAL = False
RENDER_TRAIN = False
GAMMA = 0.99
TAU = 0.005
LR = 0.0003
ALPHA = 0.2
AUTOMATIC_ENTROPY = True
SEED = 123456
BATCH_SIZE = 256
NUM_GAMES = 2000
HIDDEN_SIZE = 512
UPDATES_PER_STEP = 1
START_GAMES = 1000
TARGET_UPDATE_INTERVAL = 1
REPLAY_SIZE = 1000000
LOG_SIG_MAX = 2
LOG_SIG_MIN = -20
USE_DOUBLE_VER = True
CAP_ENTROPY_TEMP = False
ENTROPY_TEMP_MAX = 1e4
ENTROPY_TEMP_MIN = 0
SAVE_BUFFER_IN_CHECKPOINTS = True
SAVE_FREQ = 20
EPISODES_FOR_EVAL = 100
BATCH_DISTANCE_TH = 0.6
USE_BATCH_DISCRIMINATION = True
USE_DELAYED_BUFFER = True
DELAYED_BUFFER_EPISODE_SIZE = 10
MIX_MOO_EXP = True

PARENT_FOLDER_FOR_CHECKPOINTS = 'checkpoints/'
MODEL_CKPT_DEFAULT_NAME = "model_state"
BUFFER_CKPT_DEFAULT_NAME = "buffer_state"
EXPERIMENT_NICKNAME=f"sac{f'_sdp{BATCH_DISTANCE_TH}' if USE_BATCH_DISCRIMINATION else ''}{f'_moo{DELAYED_BUFFER_EPISODE_SIZE}' if USE_DELAYED_BUFFER else ''}_small_buf"#f"prioritized_buffer_sampling_th{BATCH_DISTANCE_TH}"
LOAD_CKPT_ON_TRAIN = False
TRAIN_LOAD_PATH = "checkpoints/sac_checkpoint_ArmAndPole-v0_2002.7110022083496_SAC_long_running_2_2022-03-24_19-42-57"
VIDEO_SAVE_FOLDER = f'episode_captures/capture_{EXPERIMENT_NICKNAME}_{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}/'
epsilon = 1e-6
policy = "Gaussian" if GAUSSIAN else "Deterministic"
save_folder = f'runs/{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}_SAC_{EXPERIMENT_NICKNAME}_{ENV_NAME}_{policy}{"_autotune" if AUTOMATIC_ENTROPY else ""}{"_double" if AUTOMATIC_ENTROPY else ""}'