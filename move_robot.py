# Written by Flavius Radu Modan - 2022
# Testing script for the gym - moves robot and
# prints simulation data
import time

import arm_and_pole
import gym
import mujoco_test_env
import numpy as np
from pynput.keyboard import Key, Listener

env = gym.make("ArmAndPole-v0")

action = np.array([0 for i in range(7)])
step = 1

def on_press(key):
    if key.char == "q":
        action[0] += step
    if key.char == "a":
        action[0] -= step
    if key.char == "w":
        action[1] += step
    if key.char == "s":
        action[1] -= step
    if key.char == 'e':
        action[2] += step
    if key.char == "d":
        action[2] -= step
    if key.char == "r":
        action[3] += step
    if key.char == "f":
        action[3] -= step
    if key.char == "u":
        action[6] += step
    if key.char == "j":
        action[6] -= step
    if key.char == "y":
        action[5] += step
    if key.char == "h":
        action[5] -= step

def print_contact_info(env):
    d = env.sim.data
    for coni in range(d.ncon):
        print('  Contact %d:' % (coni,))
        con = d.contact[coni]
        print(con.geom1,con.geom2,con.solimp)
    


env.reset()


listener = Listener(on_press=on_press)
listener.start()
step = 0
while True:
    env.render()
    # new_states, reward, done, info = env.step(action)
    pos = []
    pos.append(env.sim.data.get_sensor("right_j0_pos"))
    pos.append(env.sim.data.get_sensor("right_j1_pos"))
    pos.append(env.sim.data.get_sensor("right_j2_pos"))
    pos.append(env.sim.data.get_sensor("right_j3_pos"))
    pos.append(env.sim.data.get_sensor("right_j4_pos"))
    pos.append(env.sim.data.get_sensor("right_j5_pos"))
    pos.append(env.sim.data.get_sensor("right_j6_pos"))
    # if done:
    #     print( done)
    step+=1
