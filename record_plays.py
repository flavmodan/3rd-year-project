# Written by Flavius Radu Modan 2022
# Script to record videos of episode playthrough
import argparse
import datetime
import gym
import numpy as np
import itertools
import torch
from lib.algo.sac import SAC
from lib.replay_memory import ReplayMemory
import arm_and_pole
from config import *
from torch.utils.tensorboard import SummaryWriter
import cv2

env = gym.make(ENV_NAME)
memory = ReplayMemory(REPLAY_SIZE, SEED)
agent = SAC(env.observation_space.shape[0], env.action_space,memory)
agent.load_checkpoint(ckpt_folder="checkpoints/sac_checkpoint_ArmAndPole-v0_6128.577177828854_SAC_prority_buffer_sampling_better_2022-03-26_13-57-39")
#Tesnorboard
ts_writer = SummaryWriter(save_folder+"_eval",save_folder+"_eval")
total_ep = 0
frame_shape = (1920,1080)

if not os.path.exists(VIDEO_SAVE_FOLDER):
    os.makedirs(VIDEO_SAVE_FOLDER)

fourcc = cv2.VideoWriter_fourcc(*'mp4v')
zeros = np.zeros(frame_shape)
for i in range(EPISODES_FOR_EVAL):
    state = env.reset()
    episode_reward = 0
    done = False
    writer = cv2.VideoWriter(VIDEO_SAVE_FOLDER+f"run_{total_ep}.mp4",fourcc,120,(frame_shape[0]*2,frame_shape[1]*2))
    while not done:
        frame = env.sim.render(frame_shape[0]*2,frame_shape[1]*2)
        writer.write(cv2.cvtColor(cv2.flip(frame, 0), cv2.COLOR_BGR2RGB))
        action = agent.select_action(state, evaluate=True)
        next_state, reward, done, _ = env.step(action)
        episode_reward += reward
        state = next_state
    writer.release()
    print(f"Episode {total_ep} reward {episode_reward}")
    ts_writer.add_scalar('avg_reward/test', episode_reward,total_ep)
    total_ep += 1
