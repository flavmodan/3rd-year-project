# Written by Flavius Radu Modan 2022
# Testing script for env - does random actions
import time

import arm_and_pole
import gym
import mujoco_test_env
from config import *
env = gym.make("ArmAndPole-v0")
# env = gym.make("MujocoTestEnv-v0")
env.seed(SEED)
env.action_space.seed(SEED)
GAMES = 1000000

for _ in range(GAMES):
    env.reset()
    step = 0
    done = False
    while not done:
        action = env.action_space.sample()
        action = action * -0.01 + - 0.0001 * step
        new_states, reward, done, info = env.step(action)
        print(reward, done)
        # print(action)
        env.render()
        step += 1
    print()
